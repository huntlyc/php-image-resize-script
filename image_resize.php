<?php
    /* File: image_resize.php
     * Author: Huntly Cameron <huntly.cameron@gmail.com>
     * Description: By defining a maximum height or width an image can be and 
     *              the directory of images, this script will go through each
     *              image in that dirctory and resize the image whilst keeping 
     *              the correct ratio.
     * Notes: This version only supports .jpg and .png images 
     *
     * LICENSED UNDER MIT LICENSE 
     *
     * Copyright (C) 2011 by Huntly Cameron
     * 
     * Permission is hereby granted, free of charge, to any person obtaining a
     * copy of this software and associated documentation files 
     * (the "Software"), to deal in the Software without restriction, 
     * including without limitation the rights to use, copy, modify, merge, 
     * publish, distribute, sublicense, and/or sell copies of the Software, 
     * and to permit persons to whom the Software is furnished to do so, 
     * subject to the following conditions:
     * 
     * The above copyright notice and this permission notice shall be included 
     * in all copies or substantial portions of the Software.
     * 
     * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
     * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
     * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
     * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
     * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
     * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
     * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
     */

     
    //Maximum width or height of the picture
    define("MAX_IMG_SIZE", "650"); //max 650 px wide or tall.

    //The directory of the pictures you wish to resize.
    define("WORKING_DIRECTORY", "/home/huntly/Pictures/test/");
    
    //Log filename
    define("LOG_FILE_NAME", "resizer_log.log");
    
    /*Variables to keep track of how many images there are in the
     *directory and how many the script has successfully resized
     *and how many failures there have been.
     */
    $_total_images = 0;
    $_total_resized = 0;
    $_total_failed = 0;


    main();

    function main()
    {
        global $_total_images;
        //Print welcome message and confirm the user wishes to run script.
        printWelcomeMessage();
        $run_program = getUserConfirmation();

        //If the user typed in "n" then kill the script
        if(!$run_program)
        {
            die("Exiting application\n\n");
        }        

        echo "\n";
        echo "Resizing images.  This may take a while depending on the ";
        echo "number of images...\n";

        try
        {
            foreach(new DirectoryIterator(WORKING_DIRECTORY) as $file)
            {
                $extension = getFileExtension($file);

                if(isValidImage($extension))
                {   
                    $_total_images++;
                    list($img_width, $img_height) = 
                        getimagesize(WORKING_DIRECTORY . $file);
                
                    if($img_width > MAX_IMG_SIZE || 
                       $img_height > MAX_IMG_SIZE)
                    {
                        list($new_width, $new_height) = 
                            getNewImageDimensions($img_width, 
                                                  $img_height, 
                                                  $file);

                        resizeImage($file, 
                                    $extension,
                                    $img_width,
                                    $img_height,
                                    $new_width, 
                                    $new_height);
                    }
                }
            }

            printScriptSummary();
            exit;
        }
        catch(UnexpectedValueException $uve)
        {
            echo "UnexpectedValueException caught: {$uve}";
            echo "Check the working_directory value is correct.";
        }    
    }

    /*Function: printWelcomeMessage()
     *Returns: nothing.
     *
     *Outputs the program name and version along with the 
     *directory path that the script will be working on 
     *and the maximum size the width or height of an image can be
     */
    function printWelcomeMessage()
    {
        echo "\n";
        echo "image_resize v1.0\n";
        echo "=================\n\n";
        echo "Image directory: " . WORKING_DIRECTORY . "\n";
        echo "Max width or height: " . MAX_IMG_SIZE . "px\n";
    }

    /*Function: getUserConfirmation()
     *Returns: Bool representing Y/N confirmation
     *
     *Asks the user if they wish to continue.  It then checks the input to 
     *make sure the user has typed in "y" or "n", if not it asks again.
     */
    function getUserConfirmation()
    {        
        $run_program = FALSE;
        echo "Do you wish to continue? [y/n]: ";
     
        //get user input
        try
        {
            $std_in = fopen("php://stdin", "r");
            $user_input = strtolower(substr(fread($std_in,2), 0, 1));
            
            if($user_input == "y" || $user_input == "n")
            {
                if($user_input == "y")
                {
                    $run_program = TRUE;
                }
            }
            else
            {
                echo "Invalid option, try again!\n";
                getUserConfirmation();
            }
        }
        catch(Exception $exception)
        {
            echo "Exception: couldn't get user details: ";
            echo "{$exception}";
        }

        return $run_program;   
    }

    /*Function: getFileExtension(String $file)
     *Returns: String $extension
     *
     *Given a string filename, this function will return
     *the extension such as "png", "jpg" or "txt.
     */
    function getFileExtension($file)
    {        
        $extension = substr($file, -3, 3);        
        return $extension;
    }

    /*Function: isValidImage(String $extension)
     *Returns: Boolean $is_valid_image
     *
     *Checks to see if the file we have is a valid image
     *in terms of its extension.  
     */
    function isValidImage($extension)
    {
        $is_valid_image = FALSE;
        
        //if extension is .png .jpg 
        if($extension == "jpg" ||
           $extension == "png" ||
           $extension == "jpeg")
        {
            $is_valid_image = TRUE;
        }

        return $is_valid_image;
    }

    /*Function getNewImageDimensions(File $file, 
     *                               int $img_width, 
     *                               int $img_height)
     *Returns: array($new_width, $new_height)
     *
     *Takes the image file, its height and width and calculates the new 
     *dimensions required to make the image no wider or taller than the 
     *MAX_IMG_SIZE.
     */
    function getNewImageDimensions($img_width, $img_height, $file="image")
    {        
        $log_output = "File: {$file}\n";
        $log_output .= "Initial Dimensions: {$img_width} x {$img_height}\n";
        
        appendToLogFile($log_output);

        //work out the resized values
        if($img_height > $img_width)
        {            
            $new_height = MAX_IMG_SIZE;
            $resize_factor = (float) $new_height/$img_height;
            $new_width = (float) $resize_factor * $img_width;
            $new_width = ceil($new_width);
        }
        else
        {            
            $new_width = MAX_IMG_SIZE;
            $resize_factor = (float) $new_width/$img_width;
            $new_height = (float) $resize_factor * $img_height;
            $new_height = ceil($new_height);
        }

        $log_output = "Calculated new dimensions for {$file}  as: ";
        $log_output .= "{$new_width} x {$new_height}\n";

        appendToLogFile($log_output);

        return array($new_width, $new_height);
    }

    /*Function resizeImage(File $file, 
     *                     String $extension, 
     *                     int $old_width, 
     *                     int $old_height, 
     *                     int $new_width, 
     *                     int $new_height)
     *Returns: nothing
     *
     *Function takes the file, its extension, its old dimensions and its
     *new dimensions and will resize the image to the new dimensions and 
     *overwrite the old image with the newly resized one.
     */
    function resizeImage($file, 
                         $extension, 
                         $old_width, 
                         $old_height, 
                         $new_width, 
                         $new_height)
    {
        global $_total_resized, $_total_failed;        
        //create a new tmp image to transfer the old one to.
        $new_image = imagecreatetruecolor($new_width, $new_height);

        //Create image resource from $old_image.
        if($extension == "jpg" || $extension == "jpeg")
        {
            $old_image = imagecreatefromjpeg(WORKING_DIRECTORY . $file);
        }
        else if($extension == "png")
        {
            $old_image = imagecreatefrompng(WORKING_DIRECTORY . $file);
        }

        $is_img_resized = imagecopyresampled($new_image,
                                             $old_image,
                                             0,0,0,0, //no offsets
                                             $new_width,
                                             $new_height,
                                             $old_width,
                                             $old_height);
        
        if($is_img_resized)
        {
            //write to log file hat we've changed the file.
            $log_output = "Changed {$file} from [{$old_width}px X ";
            $log_output .= "{$old_height}px] to [{$new_width}px X ";
            $log_output .= "{$new_height}px]\n";
            $log_output .= "--------\n";

            appendToLogFile($log_output);

            //add to the resized count
            $_total_resized++;

            //save the image
            $fileURI = WORKING_DIRECTORY . $file;
            if($extension == "jpg")
            {
                imagejpeg($new_image, $fileURI);
            }
            else if($extension == "png")
            {
                imagepng($new_image, $fileURI);
            }
        }
        else
        {
            //Tell user of error and also write it to the log.
            $log_output = "Error: Could not resize {$file}";
            echo $log_output;            
            $_total_failed++;
            $log_output .= "--------\n";
            appendToLogFile($log_output);            
        }

        $new_image = NULL;
    }

    /*Function: appendToLogFile(String $text)
     *Returns: nothing
     *
     *Will take the $text param and write it to the defined LOG_FILE 
     */
    function appendToLogFile($text)
    {
        $log_file = WORKING_DIRECTORY . LOG_FILE_NAME;

        try
        {
            $open_file = fopen($log_file, "a");
            fwrite($open_file, $text);
            fflush($open_file);
            fclose($open_file);
        }
        catch(Exception $exception)
        {
            echo "Exception in appendToLogFile(): {$exception}";
        }
    }

    /*Function: printScriptSummary()
     *Returns: nothing
     *
     *Will print out the number of images in the directory, the number of
     *images resized and the number of failed resizes.
     */
    function printScriptSummary()
    {
        global $_total_images, $_total_resized, $_total_failed;        
        $output_message = "Finished resizing images in ";
        $output_message .= WORKING_DIRECTORY . "\nLog saved to: ";
        $output_message .= WORKING_DIRECTORY . LOG_FILE_NAME . "\n\n";
        $stats = "Stats:\n";
        $stats .= "------\n";
        $stats .= "Number of images in directory: {$_total_images}\n";
        $stats .= "Number of images resized: {$_total_resized}\n";
        $stats .= "Number of failed resizes: {$_total_failed}\n";
        appendToLogFile($stats);
        $output_message .= "{$stats}\n";
        $output_message .= "Exiting...\n";

        //output info to user.
        echo "\n";        
        echo $output_message;
    }
?>
